# SDOH Pipeline Singularity

This container is used for running the SDOH pipeline within PACE. Importantly, it contains the SDOH data repository files within. 

## Pulling to PACE

Use the following steps to pull the image into PACE:

#### 1. Log onto PACE HPC
#### 2. Set proxy variables for PACE
 ```
    export http_proxy=http://pacegateway.dhe.duke.edu:3128/
    export https_proxy=https://pacegateway.dhe.duke.edu:3128/
```
#### 3. Download the image

##### Latest version

This will pull the latest successful build.

```
export CONTAINER_DIR="/work/${USER}/GIM/CHART/containers"
mkdir -p ${CONTAINER_DIR}
wget --no-check-certificate \
    -P ${CONTAINER_DIR} \
    -O "${CONTAINER_DIR}/sdoh-pipeline-singularity_latest.sif" \
    'https://research-containers-01.oit.duke.edu/chart-consortium/sdoh-pipeline-singularity_latest.sif'
```

##### Tagged version

This will pull a build with a tag of your choice.

```
export TAG="[DESIRED-TAG-HERE]"
export CONTAINER_DIR="/work/${USER}/GIM/CHART/containers"
mkdir -p ${CONTAINER_DIR}
wget --no-check-certificate \
    -P ${CONTAINER_DIR} \
    "https://research-containers-01.oit.duke.edu/chart-consortium/sdoh-pipeline-singularity_${TAG}.sif"
```

